# ssim

![](https://img.shields.io/badge/written%20in-C%2C%20Bash%2C%20and%20VBScript-blue)

A command-line utility for objectively comparing image quality, including a short report.

Source code included in distribution.

The included report scripts encode lena.png to H.264 using x264 (single frame), to VP8 using webp, to JPG using Photoshop CS3, and to JP2K using imagemagick, at a variety of output file sizes to compare the filesize/quality tradeoff of each encoder and format.


## Download

- [⬇️ ssim-r1.7z](dist-archive/ssim-r1.7z) *(520.45 KiB)*
